package com.example.app3

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.app3.databinding.ActivityMainBinding
import com.example.app3.model.Oil
import androidx.recyclerview.widget.LinearLayoutManager

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        val data = listOf<Oil>(
            Oil("เชลล์ ฟิวเซฟ แก๊สโซฮอล์ E20", 36.84),
            Oil("เชลล์ ฟิวเซฟ แก๊สโซฮอล์ 91", 37.68),
            Oil("เชลล์ ฟิวเซฟ แก๊สโซฮอล์ 95", 37.95),
            Oil("เชลล์ วี-เพาเวอร์ แก๊สโซฮอล์ 95", 45.44),
            Oil("เชลล์ ดีเซล B20", 36.34),
            Oil("เชลล์ ฟิวเซฟ ดีเซล", 36.34),
            Oil("เชลล์ ฟิวเซฟ ดีเซล B7", 36.34),
            Oil("เชลล์ วี-เพาเวอร์ ดีเซล", 36.34),
            Oil("เชลล์ วี-เพาเวอร์ ดีเซล B7", 47.06)
        )

        //recyclerView
        val RecyclerView = binding.RecycleView
        RecyclerView.layoutManager = LinearLayoutManager(this)
        RecyclerView.adapter = ItemAdapter(data, applicationContext)
    }

    class ItemAdapter(val data: List<Oil>, val context: Context) :
        RecyclerView.Adapter<ItemAdapter.ViewHolder>() {


        inner class ViewHolder(private val itemView: View) : RecyclerView.ViewHolder(itemView) {
            val nametitle = itemView.findViewById<TextView>(R.id.TextNameOil)
            val price = itemView.findViewById<TextView>(R.id.TextPrice)
            val item = itemView.findViewById<LinearLayout>(R.id.item)
        }

        //Recycler Binding List_item
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val adapterLayout =
                LayoutInflater.from(parent.context).inflate(R.layout.list_item, parent, false)
            return ViewHolder(adapterLayout)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            var item = data[position]
            holder.nametitle.text = item.nametitle
            holder.price.text = item.price.toString()
            holder.item.setOnClickListener {
                Toast.makeText(
                    context,
                    "${item.price.toString()} ${item.nametitle}", Toast.LENGTH_LONG
                )
                    .show()
            }
        }

        override fun getItemCount(): Int {
            return data.size
        }
    }
}
